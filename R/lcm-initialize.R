

#' Initialize arrays
#'
#' Returns two arrays: \code{mat} and \code{one_run}
#' No need to run this independently. Run internally in \code{lcm_run}
#'
#' @param hab_params_df output of \code{\link{hab_run}} or \code{\link{hab_run_stochastic}}
#'
#' @export


lcm_initialize <- function(hab_params_df) {

  pop <- unique(hab_params_df$pop)

  if (length(pop) != 1) stop('Only one pop at a time to lcm_initialize')

  # Gather subbasin names
  subbasin_name <- unique(hab_params_df$subbasin)
  num_subbasin <- length(subbasin_name)

  if ('year' %in% colnames(hab_params_df)) {
    years <- unique(hab_params_df$year)

  } else {
    years <- 1:100

  }


  # Coho lifestages and sensitivity parameters
  if (pop == "coho") {
    lifestages <- c(
      'ocean0',
      'ocean1',
      'ocean2',
      'total_run',
      'spawners',
      # Above here is needed for spawner matrix
      'eggs',# Here and below are stored for diagnostics
      'pre_fry',
      'fry',
      'parr',
      'smolts',
      'fry_movers',
      'parr_movers',
      'natal_smolts',
      'non_natal_smolts'
    )

    sens.params <- c(
      "Egg.capacity",
      "Incubation.productivity",
      "Summer.rearing.productivity",
      "Summer.rearing.capacity",
      "Winter.rearing.productivity",
      "Winter.rearing.capacity",
      "Prespawn.productivity",
      "geomean")# model output
  } #end if coho




  if (grepl('chinook', pop)) {
    # either spring_chinook or fall_chinook

    lifestages <- c('ocean0',
                    'ocean1',
                    'ocean2',
                    'ocean3',
                    'ocean4',
                    'ocean5',
                    'total_run',
                    'spawners',  # Above here is needed for spawner matrix
                    'eggs', # Here and below are stored for diagnostics
                    'pre_fry',
                    'natal_fry',
                    'fry_migrants',
                    'natal_fry_distrib',
                    'sub_yr_distrib',
                    'sub_yr',
                    'fry_migrants_bay',
                    'sub_yr_bay'
    )


    sens.params <- c(
      "Egg.capacity",
      "Incubation.productivity",
      "Rearing.productivity",
      "Rearing.capacity",
      "Rearing.productivity.June",
      "Prespawn.productivity",
      "geomean")

  } # end if chinook



  if (pop == "steelhead") {

    ocean_stages <- paste0('age', 2:6, '_ocean')
    firstspawn_stages <- paste0('age', 2:6, '_firstspawn')
    kelt_stages <- paste0('age', 3:8,'_kelt')


    lifestages <- c(ocean_stages,
                    firstspawn_stages,
                    kelt_stages,
                    'total_run',
                    'spawners',
                    'eggs',
                    'pre_fry',
                    'parr',
                    'age1',
                    'age1_stayers',
                    'age1_smolts',
                    'age2_stayers',
                    'age2_smolts',
                    'age3_smolts'
    )

    sens.params <- c(
      "Egg.capacity",
      "Incubation.productivity",
      "First.summer.productivity",
      "First.summer.capacity",
      "First.winter.productivity",
      "First.winter.capacity",
      "Second.summer.productivity",
      "Second.summer.capacity",
      "Second.winter.productivity",
      "Second.winter.capacity",
      "Prespawn.productivity",
      "geomean")# model output
  } #end if steelhead


  if (pop == 'chum') {
    lifestages <- c('ocean0',
                    'ocean1',
                    'ocean2',
                    'ocean3',
                    'ocean4',
                    'ocean5',
                    'total_run',
                    'spawners',  # Above here is needed for spawner matrix
                    'eggs', # Here and below are stored for diagnostics
                    'fry',
                    'pre_fry',
                    'fry_migrant',
                    'fry_migrant_bay'
    )


    sens.params <- c(
      "Egg.capacity",
      "Incubation.productivity",
      "Rearing.productivity",
      "Prespawn.productivity",
      "geomean")

  } # end if chinook


  # Create arrays to hold fish lifestages by spatial units ----

  N <- matrix(0,
              nrow = length(lifestages),
              ncol = num_subbasin,
              dimnames = list(lifestages = lifestages, subbasin = subbasin_name)
              )



  # Store all model results in 3 dimensional array: years x lifestage x subbasin
  one_run <- array(
    NA,
    c(
      length(years),
      length(lifestages),
      num_subbasin
    ),
    dimnames = list(year = years,
                    lifestage = lifestages,
                    subbasin = subbasin_name)
  )



  # return list
  array_list <- list(mat = N,
                     one_run = one_run)

  return(array_list)

}

