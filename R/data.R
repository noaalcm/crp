#' Subbasin level parameter values.
#'
#' A dataset containing the productivity, capacity and other values
#' for each species, life stage and scenario.
#'
#' @format A tibble with 49,896 rows and 6 variables:
#' \describe{
#'   \item{pop}{population or species}
#'   \item{scenario}{which diagnostic or restoration scenario}
#'   \item{subbasin_num}{subbasin number}
#'   \item{param}{subbasin number}
#'   \item{value}{parameter value}
#' }
#' @source \describe{generated from the habitat scenario model}
"habitat_scenarios"
