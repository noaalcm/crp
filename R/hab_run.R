

#' Run the habitat model
#'
#' Create habitat scenarios
#'
#' @param run_pop which pop to run
#' @param run_scenario which scenario to run.  For asrp scenarios, choose scenario_1, scenario_2, or scenario_3
#' @param run_era which era to run. The choices are 1900, 2019, 2040, 2080.
#' @param df_temperature default is crp::temperature_df. Vary temps here
#' @param df_width default is crp::width_df. Vary widths here
#' @param df_fl all flowline reaches for the pop of interest
#'
#' @import dplyr
#' @import tidyr
#' @import stringr
#' @import purrr
#'
#' @export

hab_run <- function(run_pop,
                    run_scenario,
                    run_era,
                    df_temperature = crp::temperature_df,
                    df_width = crp::width_df[crp::width_df$pop == run_pop, ],
                    df_fl = crp::flowline_clean[crp::flowline_clean$pop == run_pop, ]){

  df_lr <- hab_clean_lr(run_pop, df_fl)

  df_w <- hab_apply_lgr_wood_mult(run_pop, df_lr)

  df_ss_clean <- hab_clean_ss(df_fl, run_scenario, run_era)

  df_ss <- hab_calc_ss_length(df_ss_clean)

  df_bw <- hab_clean_backwater(df_fl, run_scenario, run_era)

  df_lr_clean <- hab_calc_lr_length(run_pop, df_lr, run_scenario, run_era)

  df_sch <- hab_create_side_channels(df_fl)

  df_fp_clean <- hab_clean_floodplain(df_sch, df_fl, run_scenario, run_era)

  df_fp <- hab_calc_fp_area(run_pop, df_fp_clean, df_fl)

  df_ss_area <- hab_calc_ss_area(run_pop, df_width, df_ss)

  df_lr_area <- hab_calc_lr_area(run_pop, df_width, df_lr_clean, df_bw)

  df_cap <- hab_calc_capacity(run_pop, df_lr_area, df_ss_area, df_fp, df_w, df_temperature)

  df_mvmt <- hab_calc_movement(run_pop, df_ss_area, df_lr_area, df_fp, df_cap, run_scenario)

  df_prod <- hab_calc_productivity(run_pop, df_cap)

  df_splr <- hab_calc_capacity_spawn_lr(run_pop, df_fl, df_width, run_era)

  df_spss <- hab_calc_capacity_spawn_ss(run_pop, df_ss_clean, df_width)

  df_spfp <- hab_calc_capacity_spawn_fp(run_pop, df_fp_clean)

  df_splr_clean <- hab_clean_capacity_spawn_lr(run_pop, df_splr, run_scenario, run_era)

  df_cap_spawn <- hab_calc_capacity_spawn(run_pop, df_splr_clean, df_spss, df_spfp)

  df_cap_egg_weight <- hab_calc_egg_cap_weight(df_splr_clean, df_spss, df_spfp)

  df_prod_e2f <- hab_calc_productivity_egg2fry(df_fl, df_cap_egg_weight)

  df_prod_prespawn <- hab_calc_productivity_prespawn(run_pop, run_scenario, run_era, df_fl, df_cap_egg_weight, df_temperature)

  results <- hab_organize(run_pop, df_prod, df_cap_spawn, df_prod_prespawn, df_prod_e2f, df_mvmt)


  results <- tibble::add_column(results,
                                era = run_era,
                                .before = 'subbasin_num')

  message(paste("Successful run of habitat model for", run_pop, run_scenario, run_era))



  # Shade, LR and ASRP scenarios require multiple to be run
  # return only the specified scenario
  if (run_scenario %in% c(diag_scenarios, combined_scenarios)) {
    return(filter(results, scenario == run_scenario))
  } else {
    return(
      filter(results,
             scenario == paste('ASRP', run_scenario, run_era, sep = '_')
      ))
  }

}
