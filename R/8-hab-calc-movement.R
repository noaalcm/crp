#16-movement



#' Calculate movement percents
#'
#' Calculate movement percents
#'
#' @param run_pop which pop to run
#' @param df_ss Output of hab_calc_ss_area()
#' @param df_lr output of hab_clean_lr()
#' @param df_fp output of hab_calc_fp_area()
#' @param df_cap output of hab_calc_capacity()
#' @param run_scenario which scenario to run
#'
#' @export


hab_calc_movement <- function(run_pop, df_ss, df_lr, df_fp, df_cap, run_scenario){


  if (run_scenario %in% c(diag_scenarios, 'dev_and_climate', combined_scenarios)) {

    if (run_scenario %in% c('LR', 'Shade')) {

      mvmt_df <- lapply(c(run_scenario, 'Current'), function(s) {
        subbasin_names %>%
          select(subbasin_num) %>%
          mutate(scenario = s)
      }) %>%
        do.call('rbind',.)

    } else {

      mvmt_df <- subbasin_names %>%
        select(subbasin_num) %>%
        mutate(scenario = run_scenario)
    }

    mvmt_df <- mvmt_df %>%
      mutate(movement = case_when(
        scenario %in% c("Current", 'dev_and_climate', 'Barriers', 'Shade', 'LR','Fine_sediment') ~ mvmt_base,
        scenario %in% c('Floodplain', 'Beaver', 'FP_wood_comb', 'Historical', 'All_100%') ~ mvmt_nat_potential,
        scenario == 'Wood' ~ mvmt_wood,
        str_detect(scenario, '25%') ~ mvmt_base + (.25 * (mvmt_nat_potential - mvmt_base)),
        str_detect(scenario, '50%') ~ mvmt_base + (.5 * (mvmt_nat_potential - mvmt_base)),
        str_detect(scenario, '75%') ~ mvmt_base + (.75 * (mvmt_nat_potential - mvmt_base))))

  } else {

  df_density <- get(paste0('params_hab_',run_pop))$density


  mvmt_data <- bind_rows(df_ss, df_lr, df_fp) %>%
    filter(scenario %in% single_action_mvmt_scenarios)

    if (run_pop == "steelhead") {
      mvmt_data <- mvmt_data %>%
        bind_rows(mvmt_data %>%
                    filter(life.stage %in% c("summer", "winter")) %>%
                    mutate(life.stage = ifelse(life.stage == "summer",
                                               "summer.2",
                                               "winter.2")))
    }

  mvmt_data <- mvmt_data %>%
    left_join(df_density) %>%
    mutate(capacity = Area * density) %>%
    filter(life.stage %in% c("winter", "winter.2")) %>%
    group_by(subbasin_num, scenario) %>%
    summarize(cap_mvmt = sum(capacity, na.rm = T))

  mvmt_data_curr <- df_cap %>%
    filter(scenario == 'Current',
           life.stage %in% c('winter', 'winter.2')) %>%
    select(subbasin_num, capacity) %>%
    group_by(subbasin_num) %>%
    summarize(current_cap = sum(capacity, na.rm = TRUE)) %>%
    ungroup()

  mvmt_1 <- mvmt_data %>%
    left_join(mvmt_data_curr) %>%
    spread(scenario, cap_mvmt) %>%
    rename(scenario_1_cap_curr = current_cap) %>%
    mutate(scenario_2_cap_curr = scenario_1_cap_curr,
           scenario_3_cap_curr = scenario_1_cap_curr) %>%
    gather(scenario, cap, -subbasin_num) %>%
    group_by(subbasin_num) %>%
    mutate(
      scenario = case_when(
        str_detect(scenario, 'scenario_1') ~ 'scenario_1',
        str_detect(scenario, 'scenario_2') ~ 'scenario_2',
        str_detect(scenario, 'scenario_3') ~ 'scenario_3'),
      scenario = ifelse(str_detect(scenario, 'curr'),
                            'current',
                            as.character(scenario)),
      diff = cap - cap[scenario == 'current']) %>%
    group_by(subbasin_num, scenario) %>%
    mutate(tot_diff = sum(diff, na.rm = T),
           perc = case_when(
             scenario == 'current' ~ cap / (cap[scenario == 'current'] + tot_diff),
             !scenario == 'current' ~ diff / (cap[scenario == 'current'] + tot_diff)),
           curr_cap = cap) %>%
    group_by(subbasin_num, scenario) %>%
    mutate(
      perc_single_action = case_when(
        !scenario == 'current' ~ diff / (curr_cap + diff),
        scenario == 'current' ~ 0),
      base_mvmt = case_when(
        str_detect(scenario, 'wood') ~ mvmt_wood,
        str_detect(scenario, 'fp|beaver') ~ mvmt_nat_potential,
        !str_detect(scenario, 'wood|fp|beaver') ~ mvmt_base))

  mvmt_df <- mvmt_1 %>%
    group_by(subbasin_num, scenario) %>%
    summarize(movement = sum(perc * mvmt_base, na.rm = T)) %>%
    rename(scenario = scenario) %>%
    bind_rows(., mvmt_1 %>%
                filter(!scenario == 'current') %>%
                group_by(subbasin_num, scenario) %>%
                summarize(movement = sum((perc_single_action * base_mvmt) + ((1 - perc_single_action) * mvmt_base), na.rm = T))) %>%
    mutate(movement = ifelse(movement == 0,
                             NA,
                             movement))
}
  return(mvmt_df)
}
