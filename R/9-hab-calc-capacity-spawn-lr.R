# Purpose: Read in the hand digitized riffle data
# The riffle polygons are used to denote large river reaches that have one or more riffles
# Using a bankfull width and spawnable length (scaled to bankfull width) calculate a spawning
# area per noaaid

# Read in data ----

# Hand digitized riffles in the large rivers
# Summarize as number of riffles per noaaid


#' Calculate spawning capacity in large rivers
#'
#' Read in the hand digitized riffle data
#' The riffle polygons are used to denote large river reaches that have one or more riffles
#' Using a bankfull width and spawnable length (scaled to bankfull width) calculate a spawning
#' area per noaaid.
#'
#' @param run_pop which pop to run
#' @param df_flowline output of hab_clean_flowline()
#' @param df_width dataframe holding reach specific widths for each scenario
#' @param run_era which era are we running
#'
#' @export

hab_calc_capacity_spawn_lr <- function(run_pop, df_flowline, df_width, run_era){

  df_width <- filter(df_width, era == run_era, pop == run_pop)

  riff_count <- crp::riff %>%
    rename(subbasin = noaa_sub) %>%
    left_join(crp::subbasin_names %>%
                select(subbasin, subbasin_num)) %>%
    select(noaaid, subbasin_num, Shape_Area) %>%
    group_by(subbasin_num, noaaid) %>%
    summarize(riff_count = n()) %>%
    ungroup()


  # Flowline in the large river segments
  lgr <- df_flowline %>%
    left_join(df_width) %>%
    filter(Habitat == 'LgRiver',
           spawn_dist == 'Yes',
           width_s < 200) %>%
    select(noaaid, subbasin_num, Spawn_Survey, Shape_Length,
           spawn_dist, pop, both_chk, width_s, width_w, era, chino_mult) %>%
    pivot_longer(width_s:width_w,
                 names_to = 'Period',
                 values_to = 'width') %>%
    filter(ifelse(pop == 'spring_chinook',
                  Period == 'width_s',
                  Period == 'width_w')) %>%
    mutate(area_bf_m2 = Shape_Length * width)


  # If it is a reach with overlapping spring and fall chinook spawning,
  # reduce width by chino_mult. This reduction is later transferred to spawning area

  if (run_pop %in% c("spring_chinook", "fall_chinook")) {

    lgr <- lgr %>%
      mutate(width = ifelse(both_chk == "Yes",
                            width * chino_mult,
                            width))
  }

  # This is used to fill in riffle area values for reaches where we could not see the river (Spawn_Survey == N)
  ave_riff_per_reach <- inner_join(riff_count, lgr) %>%
    group_by(subbasin_num) %>%
    summarize(ave_riff_per_reach  = mean(riff_count))

  # Calculate the spawning capacity ----

  #To calculate minimum spawning area we use the BF width and a length to represent the riffle crest

  lgr_spawning_area <- lgr %>%
    left_join(riff_count) %>%
    left_join(ave_riff_per_reach) %>%
    mutate(riff_count = ifelse(is.na(riff_count), 0, riff_count),
           riff_count = ifelse(Spawn_Survey == 'N',
                               ave_riff_per_reach,
                               riff_count),
           riff_length = .5 * width,
           spawn_area = width * riff_length * riff_count) %>%
    select(spawn_area, Period, subbasin_num, noaaid)



  return(lgr_spawning_area)

}
