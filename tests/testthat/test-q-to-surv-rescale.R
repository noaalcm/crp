

test_that("Peak flow to survival function works", {

  q_1000_1900 <- q_to_surv_rescale(50000, 1900)
  q_1000_2019 <- q_to_surv_rescale(50000, 2019)
  q_1000_2040 <- q_to_surv_rescale(50000, 2040)
  q_1000_2080 <- q_to_surv_rescale(50000, 2080)

  expect_equal(q_1000_1900, q_1000_2019)
  expect_lt(q_1000_2040, q_1000_2019)
  expect_lt(q_1000_2080, q_1000_2040)

})
