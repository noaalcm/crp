# Habitat related inputs -------------------------------------------------------
# Categorize all habitat types used in the model
# Habitat types are categorized as large river, small stream, or floodplain

LgRiver_habs <-
  c(
    "Bank",
    "HM_Bank",
    "Bar_boulder",
    "Bar_gravel",
    "Bar_sand",
    "Backwater",
    'Bank_center',
    'HM_Bank_center',
    'Bar_boulder_center',
    'Bar_gravel_center',
    'Bar_sand_center'
  )

SmStream_habs <-
  c(
    "Pool",
    "Riffle",
    "Beaver.Pond"
  )

Floodplain_habs <-
  c(
    "FP_Channel",
    "Lake",
    "Marsh",
    "FP_Pond_lg",
    "FP_Pond_sm",
    "SC_pool",
    "SC_riffle",
    "Slough_lg",
    "Slough_sm",
    "Side_Channel"
  )


# Subbasin and reach related inputs --------------------------------------------
# This is a list of all mainstem subbasins

mainstem_subs <- c(52:63)

# Beaver related inputs --------------------------------------------------------
# Pond area per m is in units of ha/m
# Beaver mult is defined based on the following:
#   In historical beaver scenarios, beaver ponds take up 15% of all trib reaches
#   In current beaver scenarios, beaver ponds take up 1.375% of all trib reaches

hist_beaver_mult <- .85
hist_pond_area_per_m <- .0003
curr_beaver_mult <- .98625
curr_pond_area_per_m <- .00003

# Spawning related inputs ------------------------------------------------------

# spawning gravel multiplier for hist wood scenarios
wood_spawn_mult <- 1.3

# Pool spacing varies by slope, wood condition

# low wood, low slope reaches
psp_lwls <- 12.5
# high wood, low slope reaches
psp_hwls <- 6.25
# low wood, high slope reaches
psp_lwhs <- 27.5
# high wood, high slope reaches
psp_hwhs <- 5


# Rearing related inputs -------------------------------------------------------

# Pool ratio by slope and landcover
ss_dist_low <- tribble(~lc, ~pool_perc,
                       "Agriculture", .92,
                       "BareShrubGrass", .83,
                       "Developed", .74,
                       "Forest", .75,
                       "Wetland", .89,
                       "Reference", .81) %>%
  mutate(slope_class = "low")

ss_dist_med <- tribble(~lc, ~pool_perc,
                       "Agriculture", .60,
                       "BareShrubGrass", .50,
                       "Developed", .51,
                       "Forest", .48,
                       "Wetland", .53,
                       "Reference", .66) %>%
  mutate(slope_class = "med")

ss_dist_high <- tribble(~lc, ~pool_perc,
                        "Agriculture", .31,
                        "BareShrubGrass", .35,
                        "Developed", .54,
                        "Forest", .34,
                        "Wetland", NA,
                        "Reference", .35) %>%
  mutate(slope_class = "high")

ss_dist_ref <- tribble(~slope_class, ~pool_perc_ref,
                       "low", .81,
                       "med", .66,
                       "high", .35)

ss_dist <- bind_rows(ss_dist_low, ss_dist_med, ss_dist_high) %>%
  left_join(ss_dist_ref)



# Pool scalar used to augment winter pool area
winter_pool_scalar_warm <- .3

# Movement inputs --------------------------------------------------------------
mvmt_base <- 11
mvmt_wood <- 7
mvmt_nat_potential <- 3

# Other inputs -----------------------------------------------------------------

bw_scalar <- .16 # see Tim's description in Trello card, V4

# Define all pops run through model
pops_all <- c('coho', 'spring_chinook', 'fall_chinook', 'steelhead')

# Define all eras run through model
eras_all <- c(1900, 2019, 2040, 2080)

# Side channel width is set to 2m in the model
width_sc <- 2

# Divide area (m^2) by 10000 to convert from m^2 to ha
m_sq_per_ha <- 10000

# Some scenarios call for restoration of primary creek only --------------------
# See asrp scenario spreadsheet for list of subbasins that follow this rule
primary_cr_list <-
  c(
    "Decker-",
    "Bingham-",
    "Cedar-",
    "Sherman-",
    "Elk Cr-",
    "Crim Cr-",
    "Stillman-",
    "Big (Hump)-",
    "Stevens-",
    "Johns-",
    "Cloquallum-",
    "Porter-",
    "Scatter Cr-",
    "Beaver Cr-",
    "Lincoln Cr-",
    "Elk Cr-",
    "Elk Cr-",
    "Dry Run-",
    "Black (Wynoochee)-",
    "(Wynoochee Resevoir)",
    "Wynoochee-",
    "Waddell Cr-",
    "Garrard Cr-",
    "Bunker-",
    "Skookumchuck-",
    "Hanaford-",
    "Sterns Cr-",
    "Thrash Cr-",
    "Lake Cr-"
  )

# Create list of primary creek Reach names
primary_cr <- lapply(primary_cr_list, function(z) {
  grep(z, flowline$Reach, value = TRUE)
}) %>%
  unlist()



usethis::use_data(mainstem_subs,
                  bw_scalar,
                  wood_spawn_mult,
                  hist_beaver_mult,
                  hist_pond_area_per_m,
                  curr_beaver_mult,
                  curr_pond_area_per_m,
                  winter_pool_scalar_warm,
                  mvmt_base,
                  mvmt_wood,
                  mvmt_nat_potential,
                  psp_lwls,
                  psp_hwls,
                  psp_lwhs,
                  psp_hwhs,
                  ss_dist,
                  LgRiver_habs,
                  SmStream_habs,
                  Floodplain_habs,
                  pops_all,
                  eras_all,
                  width_sc,
                  primary_cr,
                  m_sq_per_ha,
                  overwrite = TRUE)
