# First run data-raw/hab_read_datasets.R

flowline_clean <- flowline %>%
select(-curr_temp, -hist_temp, -tm_2040, -tm_2080, -noaction_2040, -noaction_2080, -culv_list, -pass_tot) %>%
  gather(pop, spawn_dist, coho:steelhead) %>%
  left_join(chinook_mult) %>%
  mutate(chino_mult = case_when(.$pop == 'fall_chinook' ~ perc_fall,
                                .$pop == 'spring_chinook' ~ perc_spr,
                                TRUE ~ 1))

scenarios_df <- scenarios_df %>%
  select(-c(Floodplain, LW, Beaver, Riparian, Barriers))


usethis::use_data(
  flowline_clean,
  scenarios_df,
  overwrite = TRUE,
  compress = 'xz'
)

