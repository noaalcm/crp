# Raw spatial model outputs

library(tidyverse)

# Spatial model outputs --------------------------------------------------------
# Files read from most recent Chehalis spatial model outputs folder iteration
# If spatial model is re-run, copy spatial model output files to
# "data-raw/data_hab/hab_input_data/spatial_model_outputs" and re-run
# "data-raw/create-data.R"

# Culverts ----
culvs <- list.files(path = "data-raw/data_hab/hab_input_data/spatial_model_outputs",
                    pattern = "culvs_gsu_",
                    full.names = T) %>%
  read.csv() %>%
  mutate(noaa_culv = as.numeric(row.names(.)),
         FishPass = as.numeric(as.character(FishPass))/100) %>%
  select(noaa_culv, FeatureTyp,FishPass,OBS_UNIQ, GSU)

# Flowline ----
flowline <- list.files("data-raw/data_hab/hab_input_data/spatial_model_outputs",
                       pattern = "flowline",
                       full.names = T) %>%
  read.csv(.) %>%
  select(-c(mn_precip, log_area, left_buff, right_buff, can_ang,
         hist_ang, sed_current, sed_hist, forest, Seg, MWMT,
         noaa_sub_nofp, UniqueID, OID)) %>%
  rename(coho = cohospawn,
         fall_chinook = fallspawn,
         chum = chumspawn,
         spring_chinook = sprspawn,
         steelhead = steelspawn) %>%
  mutate(both_chk = ifelse(fall_chinook == "Yes" & spring_chinook == "Yes",
                           "Yes",
                           "No"),
         Reach_low = tolower(Reach),
         subbasin_num = ifelse(Habitat == "LgRiver", noaa_sub_num, noaa_sub_nofp_num),
         Habitat = case_when(!str_detect(Reach, '^Chehalis-') ~ as.character(Habitat),
                             str_detect(Reach, '^Chehalis-') & subbasin_num == 49 ~ 'Tidal',
                             str_detect(Reach, '^Chehalis-') & !subbasin_num == 49 ~ 'LgRiver'))

# Large river spawning riffles.  Hand digitized ----
riff <- list.files(path = "data-raw/data_hab/hab_input_data/spatial_model_outputs",
                   pattern = "Riffles",
                   full.names = T) %>%
  read.csv(.)

# Large river habitat data ----
LgRiver_raw <- list.files(path = "data-raw/data_hab/hab_input_data/spatial_model_outputs",
                          pattern = "LgRiver",
                          full.names = T) %>%
  read.csv(.) %>%
  mutate(source_hab = "LgRiver",
         Habitat = case_when(HabUnit %in% c("Bank", "Bank-TM") ~ "Bank",
                             HabUnit %in% c("Bank HM", "Bank HM-TM") ~ "HM_Bank",
                             HabUnit %in% c("Bar-boulder", "Bar-boulder-TM") ~ "Bar_boulder",
                             HabUnit %in% c("Bar-gravel", "Bar-gravel-TM") ~ "Bar_gravel",
                             HabUnit %in% c("Bar-sand", "Bar-sand-TM") ~ "Bar_sand"),
         Period = ifelse(Period == " Hist",
                         "Hist",
                         as.character(Period))) %>%
  filter(!Reach == 'Wynoochee-25 (Wynoochee Resevoir)') %>%
  select(-c(OID, Join_Count, TARGET_FID, Basin_wau, Wtrbdy_wau, Shape_Length_1,
         noaa_sub_num, noaa_sub, NEAR_FID, NEAR_DIST, source_hab))

# Backwater Habitat data ----
Backwater_raw = list.files(path = "data-raw/data_hab/hab_input_data/spatial_model_outputs",
                           pattern = "Backwater",
                           full.names = T) %>%
  read.csv  %>%
  mutate(Period = "Both") %>%
  select(-c(Basin_wau, Wtrbdy_wau, Shape_Length_1, Shape_Area_1, Shape_Length,
         Shape_Area, NEAR_DIST, NEAR_FID, noaa_sub, noaa_sub_num, OID,
         Join_Count, TARGET_FID, ET_ID))

# Floodplain habitat data ----
Floodplain_raw <- list.files(path = "data-raw/data_hab/hab_input_data/spatial_model_outputs",
                             pattern = "Floodplain",
                             full.names = T) %>%
  read.csv(.) %>%
  select(-c(OID, Join_Count, TARGET_FID, Shape_Length_1, Shape_Area_1, noaa_sub,
            noaa_sub_num, Shape_Area))


# Other inputs -----------------------------------------------------------------


# Large river length multipliers ----
lr_length_raw <- read.csv("data-raw/data_hab/hab_input_data/LR_Length.csv") %>%
  rename(Reach = reach)


# EDT summer and winter wetted widths. Contains current, historical, 2040 and 2080 widths ----
edt_width_raw <- read.csv("data-raw/data_hab/hab_input_data/edt_width.csv") %>%
  mutate(Reach_low = tolower(Reach)) %>%
  rename(era = year)


# ASRP scenarios ----
# Chehalis River Basin Aquatic Species Restoration Plan scenarios
asrp_scenarios_raw <- read.csv("data-raw/data_hab/hab_input_data/ASRP_scenarios.csv") %>%
  select(-c(Notes, Analog_name))

# Future impervious area ----
fut_imperv <- read.csv('data-raw/data_hab/hab_input_data/future_impervious.csv') %>%
  rename(mid_century_imperv = Mid.century.Added.Impervious.Area,
         late_century_imperv = Late.century.Added.Impervious.Area) %>%
  select(GSU, mid_century_imperv, late_century_imperv) %>%
  mutate(
    mid_century_imperv = as.numeric(gsub("%", "", mid_century_imperv)) / 100,
    late_century_imperv = as.numeric(gsub("%", "", late_century_imperv)) / 100) %>%
  gather(era, future_imperv, mid_century_imperv, late_century_imperv) %>%
  mutate(era = case_when(
    era == 'mid_century_imperv' ~ 2040,
    era == 'late_century_imperv' ~ 2080
  )) %>%
  group_by(era, GSU) %>%
  summarize(future_imperv = sum(future_imperv, na.rm = T))


usethis::use_data(lr_length_raw,
                  riff,
                  LgRiver_raw,
                  Backwater_raw,
                  Floodplain_raw,
                  fut_imperv,
                  overwrite = TRUE)
