# Create "barriers_df", input to crp that holds barrier related information ----
fl_culvs <- flowline %>%
  select(noaaid, Reach, culv_list) %>%
  filter(culv_list != "") %>%
  mutate(culv_list = str_split(culv_list,',')) %>%
  unnest(culv_list) %>%
  mutate(noaa_culv = as.numeric(culv_list)) %>%
  left_join(., culvs %>%
              select(-GSU)) %>%
  group_by(noaaid) %>%
  summarize(pass_tot = prod(FishPass),
            pass_tot_natural = prod(ifelse(FeatureTyp == 'Natural',
                                           FishPass,
                                           1))) %>%
  ungroup() %>%
  right_join(., flowline %>%
               select(-pass_tot)) %>%
  replace_na(list(pass_tot = 1,
                  pass_tot_natural = 1)) %>%
  ungroup()

barriers_df <- scenarios_df %>%
  left_join(., fl_culvs %>%
              select(noaaid, GSU, pass_tot_natural, pass_tot)) %>%
  mutate(pass_tot_scenario = ifelse(Barriers == 'y',
                                pass_tot_natural,
                                pass_tot)) %>%
  select(noaaid, pass_tot_scenario, scenario)

usethis::use_data(
  barriers_df,
  overwrite = TRUE
)

