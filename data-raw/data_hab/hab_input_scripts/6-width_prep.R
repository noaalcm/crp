# Set up width dataframe with all pops included --------------------------------
edt_width_all <- pops_all %>%
  set_names(pops_all) %>%
  map_dfr(~ edt_width_raw, .id = 'pop')

# Choose which month to use for width data--------------------------------------
# Month differs by species based on lifestage timing
edt_width <- edt_width_all %>%
  mutate(width_w = X1,
         width_s = case_when(.$pop == 'chum' ~ X3,
                             .$pop %in% c('spring_chinook', 'fall_chinook') ~ X5,
                             TRUE ~ X8)) %>%
  select(Reach_low, width_w, width_s, era, pop) %>%
  group_by(Reach_low, pop) %>%
  mutate(width_s_curr = ifelse(era == 2019,
                               width_s,
                               0),
         width_s_curr = sum(width_s_curr),
         width_s = ifelse(era %in% c(2040, 2080),
                          width_s_curr * .95,
                          width_s)) %>%
  select(-width_s_curr)

width_df_setup <- pops_all %>%
  set_names(pops_all) %>%
  map_dfr(~flowline %>%
            select(noaaid, Reach_low, wet_width),
          .id = 'pop') %>%
  left_join(., edt_width %>%
              pivot_longer(cols = width_w:width_s, names_to = 'stage', values_to = 'width') %>%
              mutate(stage = case_when(era == 2019 & stage == "width_s" ~ "width_s",
                                       era == 2019 & stage == "width_w" ~ "width_w",
                                       era == 1900 & stage == "width_s" ~ "width_s_hist",
                                       era == 1900 & stage == "width_w" ~ "width_w_hist",
                                       era == 2040 & stage == "width_s" ~ "width_s_2040",
                                       era == 2040 & stage == "width_w" ~ "width_w_2040",
                                       era == 2080 & stage == "width_s" ~ "width_s_2080",
                                       era == 2080 & stage == "width_w" ~ "width_w_2080")) %>%
              select(-era) %>%
              pivot_wider(names_from = stage, values_from = width),
            by = c('Reach_low', 'pop')) %>%
  mutate(
    width_s = ifelse(is.na(width_s),
                     wet_width,
                     width_s),
    width_w = ifelse(is.na(width_w),
                     wet_width,
                     width_w),
    width_s_hist = ifelse(is.na(width_s_hist),
                          wet_width,
                          width_s_hist),
    width_w_hist = ifelse(is.na(width_w_hist),
                          wet_width,
                          width_w_hist))

width_df <- eras_all %>%
  set_names(eras_all) %>%
  map_dfr(~width_df_setup, .id = 'era') %>%
  rename(width_s_curr = width_s,
         width_w_curr = width_w) %>%
  mutate(era = as.numeric(era),
         width_s = case_when(era == 1900 ~ width_s_hist,
                             era == 2019 ~ width_s_curr,
                             era == 2040 ~ width_s_2040,
                             era == 2080 ~ width_s_2080),
         width_w = case_when(era == 1900 ~ width_w_hist,
                             era == 2019 ~ width_w_curr,
                             era == 2040 ~ width_w_2040,
                             era == 2080 ~ width_w_2080),
         width_s = ifelse(is.na(width_s),
                          wet_width,
                          width_s),
         width_w = ifelse(is.na(width_w),
                          wet_width,
                          width_w)) %>%
  select(noaaid, era, width_s, width_w, pop)

# Parameters used to calculate width of lr edge habitat ------------------------
lr_width_scalar_bank <- 0.084
lr_width_intercept_bank <- 0.33
lr_width_scalar_hm_bank <- 0.089
lr_width_intercept_hm_bank <- 0.33
lr_width_scalar_bar <- 0.087
lr_width_intercept_bar <- 2.11


usethis::use_data(
  width_df,
  lr_width_scalar_bank,
  lr_width_scalar_bar,
  lr_width_scalar_hm_bank,
  lr_width_intercept_bank,
  lr_width_intercept_bar,
  lr_width_intercept_hm_bank,
  overwrite = TRUE,
  compress = 'xz'
)
