# Spawning ---------------------------------------------------------------------
fecundity = 3200
redd_area = 2.3
defended_redd_area = 42.3
adult_per_redd = 1.9



# Rearing ----------------------------------------------------------------------

# Capacity
density <- tribble(~Habitat, ~summer, ~fry.colonization, ~summer.2, ~winter.2,
                   "Bank", 0, 13300, 0, 0,
                   "HM_Bank", 0, 13300, 0, 0,
                   "Bar_boulder", 0, 17000, 0, 0,
                   "Bar_gravel", 0, 17000, 0, 0,
                   "Bar_sand", 0, 6700, 0, 0,
                   "Backwater", 0, 30000, 0, 0,
                   "Pool", 0, 30000, 0, 0,
                   "Riffle", 0, 30000, 0, 0,
                   "Beaver.Pond", 0, 0, 0, 0,
                   "Pool_low_ag", 0, 30000, 0, 0,
                   "FP_Channel", 0, 0, 0, 0,
                   "Lake", 0, 0, 0, 0,
                   "Marsh", 0, 0, 0, 0,
                   "FP_Pond_lg", 0, 0, 0, 0,
                   "FP_Pond_sm", 0, 0, 0, 0,
                   "SC_pool", 0, 0, 0, 0,
                   "SC_riffle", 0, 0, 0, 0,
                   "Slough_lg", 0, 0, 0, 0,
                   "Slough_sm", 0, 0, 0, 0) %>%
  pivot_longer(cols = summer:winter.2,
               names_to = 'life.stage',
               values_to = 'density')

# Productivity
p_weekly <- 0.8

surv_summer <- tribble(~Habitat, ~surv_base, ~wood_surv_base,
                           "Bank", p_weekly^2, p_weekly^2,
                           "HM_Bank", p_weekly^2, p_weekly^2,
                           "Bar_boulder", p_weekly^2, p_weekly^2,
                           "Bar_gravel", p_weekly^2, p_weekly^2,
                           "Bar_sand", p_weekly^2, p_weekly^2,
                           "Backwater", p_weekly^2, p_weekly^2,
                           "Pool", p_weekly^2, p_weekly^2,
                           "Riffle", p_weekly^2, p_weekly^2,
                           "Beaver.Pond", p_weekly^2, p_weekly^2,
                           "FP_Channel", p_weekly^2, p_weekly^2,
                           "Lake", p_weekly^2, p_weekly^2,
                           "Marsh", p_weekly^2, p_weekly^2,
                           "FP_Pond_lg", p_weekly^2, p_weekly^2,
                           "FP_Pond_sm", p_weekly^2, p_weekly^2,
                           "SC_pool", p_weekly^2, p_weekly^2,
                           "SC_riffle", p_weekly^2, p_weekly^2,
                           "Slough_lg", p_weekly^2, p_weekly^2,
                           "Slough_sm", p_weekly^2, p_weekly^2) %>%
  mutate(life.stage = 'summer')

surv_fry_colonization <- tribble(~Habitat, ~surv_base, ~wood_surv_base,
                                 "Bank", p_weekly, p_weekly,
                                 "HM_Bank", p_weekly, p_weekly,
                                 "Bar_boulder", p_weekly, p_weekly,
                                 "Bar_gravel", p_weekly, p_weekly,
                                 "Bar_sand", p_weekly, p_weekly,
                                 "Backwater", p_weekly, p_weekly,
                                 "Pool", p_weekly, p_weekly,
                                 "Riffle", p_weekly, p_weekly,
                                 "Beaver.Pond", p_weekly, p_weekly,
                                 "FP_Channel", p_weekly, p_weekly,
                                 "Lake", p_weekly, p_weekly,
                                 "Marsh", p_weekly, p_weekly,
                                 "FP_Pond_lg", p_weekly, p_weekly,
                                 "FP_Pond_sm", p_weekly, p_weekly,
                                 "SC_pool", p_weekly, p_weekly,
                                 "SC_riffle", p_weekly, p_weekly,
                                 "Slough_lg", p_weekly, p_weekly,
                                 "Slough_sm", p_weekly, p_weekly) %>%
  mutate(life.stage = 'fry.colonization')

survival <- bind_rows(surv_summer, surv_fry_colonization)

# Prespawn productivity --------------------------------------------------------
prespawn_surv_raw <- 1

# Wood multipliers for large river ---------------------------------------------
# Summer
lr_wd_s_bank <- 1
lr_wd_s_bar <- 1

# Winter
lr_wd_w_bank <- 1
lr_wd_w_bar <- 1




params_hab_chum <- dplyr::lst(fecundity,
                              redd_area,
                              adult_per_redd,
                              density,
                              survival,
                              lr_wd_s_bank,
                              lr_wd_s_bar,
                              lr_wd_w_bank,
                              lr_wd_w_bar,
                              prespawn_surv_raw)


usethis::use_data(params_hab_chum, overwrite = TRUE)
