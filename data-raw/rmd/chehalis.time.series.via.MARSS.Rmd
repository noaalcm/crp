---
title: "Chehalis.time.series.via.MARSS"
author: "Jeff Jorgensen (NWFSC)"
header-includes:
- \usepackage{float}
- \usepackage{fancyhdr}
- \pagestyle{fancy}
#- \fancyhead[CO,CE]{\bf{\emph{DRAFT}}}
- \fancyhead[L]{}
- \fancyhead[R]{}
- \fancyfoot[C]{\bf{\emph{DRAFT}}}
- \fancyfoot[R]{\thepage}
- \raggedright
- \setlength{\parindent}{1em}
- \setlength{\parskip}{1em}
- \usepackage{lineno}
output:
  pdf_document:
    latex_engine: xelatex
    number_sections: yes
    toc: yes
    toc_depth: 6
  html_document:
    depth: 6
    number_sections: yes
    theme: readable
    toc: yes
    toc_float:
      collapsed: yes
  word_document:
    toc: yes
fontsize: 11pt
indent: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
set.seed(99)
```

***`r format(Sys.time(), "%Y %B %d %X %Z")`***

# Introduction

To simulate time series while preserving correlations and covariances between them, we can use MARSS.

# Data

The following represent time series to forward simulate.

```{r load-pkg}
#
require(MARSS)
library(tidyverse)

```



We will use Porter peak and low flow data. We we also use a Chehalis weather station for TMAX air temperature as there is a relationship between 7DADM water temperature and air temperature near Chehalis (daily summary data from NCDC, station ID USC00451276).

The peak flow data will be summarized for the incubation period for each population. 

```{r spp incubation dates}

# Dates from "Aquatic-Species-Enhancement-Plan-Appendices_2014.pdf"
# Table C-2, page 45

# coho Nov 15 - May 15 (320-136) 181 days
# spring Sep 1 - April 15 (245-106) 226 days
# fall Oct 1 - May 1 (275-122) 216 days
# sthd feb 15 - Sep 15 (46-259) 152 days

incubation_range <- list(
  c(pop = 'coho',           start = "1115", end = "0315"), 
  c(pop = 'spring_chinook', start = "0901", end = "0415"),
  c(pop = 'fall_chinook',   start = "1001", end = "0501"),
  c(pop = 'steelhead',      start = "0215", end = "0915")
)
```


```{r porter-daily-Q, fig.cap = "Daily flow at Porter."}
# get-real-data
#
# Porter flow
# web page: http://usgs-r.github.io/EGRET/
# daily discharge
require(EGRET)
siteNumber.Porter <- 12031000#
startDate <- "" # Get earliest date
endDate <- "2019-09-30"
Daily.porter <- readNWISDaily(siteNumber.Porter,"00060",startDate,endDate) 
# take 1953 through 2018 for completeness
plot(Daily.porter$Date, Daily.porter$Q, type="l")# Q = m^3/s


# Find the annual maximum peak flow during the incubation period for each pop
# Subset data to species specific range, return annual max

porter.q.peak <- map_dfr(incubation_range, function(r){
  
  Daily.porter %>%
    mutate(monthday = format(Date, "%m%d"),
           pop = r['pop']) %>%
    filter(monthday <= r['start'] | monthday >= r['end']) %>%
    group_by(pop, waterYear) %>%
    summarize(Q_max = max(Q) * 35.31467)
  
}) 

porter.q <- pivot_wider(porter.q.peak,
                        names_from = pop, 
                        names_prefix = 'peak.q.',
                        values_from = Q_max)


porter.low.q7 <- tapply(Daily.porter$Q7[Daily.porter$waterYear>1952], 
                              Daily.porter$waterYear[Daily.porter$waterYear>1952], 
                              function(x, n = 7) {
                                min(as.vector(stats::filter(x, rep(1/n, n),sides=2)), 
                                    na.rm = TRUE)
                              })

porter.low.q7 <- data.frame(waterYear = names(porter.low.q7),
                            low.q7 = porter.low.q7 * 35.31467)


porter.q <- merge(porter.q, porter.low.q7)
```


```{r GM-water-T}

# Grand Mound temperature
# Used to create relationship between max air temp and max water temp

require(dataRetrieval)#https://pubs.usgs.gov/tm/04/a10/pdf/tm4A10_appendix_1.pdf
siteNumber.GM <- "12027500" #Grand Mound USGS gage 
Daily.GM <- readNWISdv(siteNumber.GM, parameterCd = "00010", statCd = c("00001", "00002"))
# take 1953 - 
Daily.GM$year <- as.numeric(substr(Daily.GM$Date, start=1, stop=4))

gm.t <- data.frame(year = as.numeric(names(tapply(Daily.GM[,4], 
                                                  as.numeric(substr(Daily.GM$Date, 
                                                                    start=1, stop=4)), 
                                                  function(x) min(x)))),
                   T7dadm = tapply(Daily.GM[,4], Daily.GM$year,
                                   function(x, n = 7) {
                                     max(as.vector(
                                       stats::filter(x, 
                                                     rep(1/n, n),
                                                     sides=2)), 
                                       na.rm=TRUE)}
                   ))

# temperature data:
### another package that is useful: https://usgs-r.github.io/dataRetrieval/
require(rnoaa)
# # list of datasets
# ncdc_datasets(token =  "Your Token Goes Here")
# # Normals Daily GHCND:USW00014895 dly-tmax-normal
#  This way can only extract 1 year at a time:
# test <- ncdc(datasetid='GHCND',#'NORMAL_DLY', 
#              stationid='GHCND:USC00451276',
#    datatypeid='TMAX',#'dly-tmax-normal', 
#    add_units = TRUE,
#  #  startdate = '1902-01-01',#'2010-05-01', 
# #   enddate = '2020-08-31',#'2010-05-10',
#    token =  "Your Token Goes Here")
# This way can extract multiple years:
require(purrr)
t.max <- map(c("USC00451276"), ghcnd_search,
             var = 'TMAX', 
             date_min = '1893-01-01', 
             date_max = '2020-8-31') %>%  

  map_dfr("tmax")
t.max <- as.data.frame(t.max)

t.max$year <- as.numeric(substr(t.max$date, 
                                start=1, stop=4))

t.max.yr <- data.frame(year = as.numeric(names(tapply(t.max$tmax, 
                                                      t.max$year, 
                                                      function(x) max(x, na.rm=TRUE)))),
                       tmaxC = tapply(t.max$tmax, 
                                                      t.max$year, 
                                                      function(x) max(x, na.rm=TRUE)*0.10))

```

We fit a relationship between 7DADM at Grand Mound (1954 - 1974, skipping 1955 and 1960) and TMAX air temperature. It has a reasonable fit with an $R^2$ = 0.58.



```{r fit-7DADM-airTMAX-relationship}

# Fit relationship between water temp at GM and Chehalis air TMAX
#   this relationship is used to convert the MARSS simulations 
#   of air TMAX to water 7DADM
#   in the life cycle model
# 1954 - 1974, to match gm.t 
# remove 1953 because it isn't a full year of readings
#  remove 1955 b/c reading is strange that year
#  remove 1960 because air T was declining but 7DADM was not and 
#  should be cooler
mydat.temp.air <- merge(x=gm.t[gm.t$year > 1953 & 
                                 gm.t$year != 1955 & 
                                 gm.t$year != 1960,], 
                        y=t.max.yr, all.x = TRUE)#
# show data
knitr::kable(mydat.temp.air)
lm.7DADM <- lm(T7dadm ~ tmaxC, mydat.temp.air)
summary(lm.7DADM)
```

```{r lm-airTMAX-T7DADM-plot, fig.cap = "Linear relationship between air TMAX and water 7DADM."}
plot(mydat.temp.air$tmaxC, mydat.temp.air$T7dadm, 
     xlab="TMAX Air (degC)", ylab="T7DADM water", las=1)
abline(lm.7DADM)
```


```{r airTMAX-T7DADM-fitted-plot, fig.cap = "Model fits includes the data (black points), predictions (horizontal bars), confidence (red line segments) and prediction (gray line segments) intervals."}
# fitted, observed, confidence & prediction intervals
plot(mydat.temp.air$year, mydat.temp.air$T7dadm,
     type="n", ylim=c(15,30), las = 1, axes = FALSE,
     xlab="Year", ylab = "7DADM (degC)")
axis(1, at=1954:1974, labels = 1954:1974)
# text(x = 1954:1974, xpd=NA,
#      y = 12, srt = 45, 1954:1974)
axis(2, las = 1)
#lines(mydat.temp.air$year, fitted(lm.7DADM))
#fitted
segments(x0 = mydat.temp.air$year-0.3,
         x1 = mydat.temp.air$year+0.3,
         y0 = predict(lm.7DADM),
         y1 = predict(lm.7DADM),
         lwd = 2, col = "black")
#prediction intervals
segments(x0 = mydat.temp.air$year,
         x1 = mydat.temp.air$year,
         y0 = predict(lm.7DADM, newdata=as.data.frame(mydat.temp.air), interval="p")[,2],
         y1 = predict(lm.7DADM, newdata=as.data.frame(mydat.temp.air), interval="p")[,3],
         lwd = 4, col = "gray70")
#confidence intervals
segments(x0 = mydat.temp.air$year,
         x1 = mydat.temp.air$year,
         y0 = predict(lm.7DADM, newdata=as.data.frame(mydat.temp.air), interval="c")[,2],
         y1 = predict(lm.7DADM, newdata=as.data.frame(mydat.temp.air), interval="c")[,3],
         lwd = 5, col ="orangered")
# observations
points(mydat.temp.air$year, mydat.temp.air$T7dadm,
       pch=21, bg="black")



```



```{r porter-low-Q-plot, fig.cap = "Porter low flow Q7."}
##   plot-real-data in a series of plots
#  with multipliers to convert m^3/s to cubic feet per second
#  and thousand cfs
# from EGRET's printqUnitCheatSheet:
plot(porter.q$waterYear, porter.q$low.q7*35.31467, type="l",
     ylab = "Porter low Q (cfs)", las=1)

```

```{r porter-peak-Q-plot, fig.cap = "Porter peak flow."}

ggplot(porter.q.peak) +
  theme_classic() +
  geom_path(aes(waterYear, 
                Q_max, 
                group = pop, 
                color = pop)) +
  labs(title = 'Porter peak flow during incubation',
       x = "water year", 
       y = "peak flow")

```


```{r gm-temperature-plot, fig.cap = "Grand Mound 7DADM water temperature."}
plot(gm.t$year, gm.t$T7dadm, type = "l",
     ylab="7DADM (GM, degC)",
     xlim=c(min(porter.q$waterYear), max(porter.q$waterYear)), las=1)

```

```{r porter-low-Q7-plot, fig.cap = "Porter low flow Q7."}

plot(porter.q$low.q7[porter.q$waterYear > 1952 & porter.q$waterYear < 1973]*35.31467, 
     gm.t$T7dadm[gm.t$year > 1952 & gm.t$year < 1973],
     xlab="Porter low.q7", ylab="GM 7DADM", las=1)
text(x=200, y=16, paste("correlation = ", 
            round(cor(porter.q$low.q7[porter.q$waterYear > 1952 & porter.q$waterYear < 1973]*35.31467, 
     gm.t$T7dadm[gm.t$year > 1952 & gm.t$year < 1973]), digits=3)))
```

 
```{r airTMAX-plot, fig.cap = "Chehalis air TMAX"}
 plot(t.max.yr$year[t.max.yr$year > 1910], t.max.yr$tmaxC[t.max.yr$year > 1910], type="l", las=1,
      xlab="Year", ylab="Air TMAX (C)", main = "Chehalis stationid USC00451276")
```

```{r airTMAX-density-plot, fig.cap = "Chehalis air TMAX distribution of observations."}
 # data are approximately normal
 plot(density(t.max.yr$tmaxC[t.max.yr$year > 1910]))

```

# Prepare data, fit MARSS model

We will use peak and low flows at Porter, and the maximum air temperature at Chehalis that will then be used in the fitted relationship with Grand Mound 7DADM above. 
Before bringing data into a MARSS model, we center and scale the data. But first, we log the peak flow data as it appears to be lognormally distributed, and the temperature and low flow data appear to be approximately normal so are not log transformed. 

As a note, we will use data after 1975 for the MARSS model fit. First, the data window used in the 7DADM relationship was from 1954-1974, but there were some observed problems between the air and water temperatures which led us to exclude some years from the model fitting, which included 1955 & 1960. Porter flows from 1973 - 1974 are missing. MARSS does not permit missing values. Also, more recent observations are not complete so we will cap off the time window at 2018.

Next, we fit a MARSS model to the data. Then, we look at some diagnostics of the model fit.

```{r MARSS-prep}
# prepare data for MARSS time window: 1975 - 2018

# water flow (peak and low flow) at Porter
#  peak flow seems to be lognormally distributed
porter <- porter.q[porter.q$waterYear > 1975 & porter.q$waterYear < 2019, ]


# Coho and chinook have a 1:1 match, so drop chinook columns

porter[,c(2,5)]  <- apply(porter[,c(2,5)], 2, log) # ln transform all peak.q cols

#porter$peak.q <- log(porter$peak.q)# ln transform
porter <- scale(porter[,c(2,5,6)])
# column means & stdevs needed later for back-transforming:
porter.mean <- attributes(porter)$'scaled:center'
porter.std <- attributes(porter)$'scaled:scale'
porter <- t(porter)


# TMAX air temperature @ Chehalis
air.T <- t.max.yr[t.max.yr$year > 1975 & t.max.yr$year < 2019, ]
air.T <- scale(air.T[, 2])
# column means & stdevs needed later for back-transforming:
air.T.mean <- attributes(air.T)$'scaled:center'
air.T.std <- attributes(air.T)$'scaled:scale'
air.T <- t(air.T)

dat.full <- rbind(porter, air.T)#rbind(t(gm), t(porter))
rownames(dat.full)[4] <- 'air.T'
colnames(dat.full) <- 1976:2018
numYears<-ncol(dat.full)
#pops <- c('coho', 'spring_chinook', 'fall_chinook', 'steelhead')
```


```{r MARSS-fit}
dat <- dat.full

Vars <- row.names(dat)
numVars <- length(Vars)
years <- 1976:2018

# MARSS fit
mod.list.noint <- list(
  B="diagonal and unequal",
  U="zero",
  Q="unconstrained",
  Z="identity",
  A="zero",
  R="zero",
  x0="zero",
  tinitx=0 )


ts.ch.1 <- MARSS(dat, 
                 model = mod.list.noint, 
                 fun.kf ="MARSSkfss", 
                 control=list(maxit=5000, 
                              conv.test.slope.tol=0.001, 
                              trace=1, 
                              safe=TRUE))
```



```{r MARSS-fit-resids-plot, fig.cap = "MARSS model residuals (innovations) with intervals.", eval= FALSE}
############### Look at residuals  ##############
# Make a plot of the model residuals (innovations) with intervals

d <- residuals(ts.ch.1, type="tt1")
d$.conf.low <- d$.fitted+qnorm(0.05/2)*d$.sigma
d$.conf.up <- d$.fitted-qnorm(0.05/2)*d$.sigma
ggplot(data = d) +
  geom_line(aes(t, .fitted)) +
  geom_point(aes(t, value), na.rm=TRUE) +
  geom_ribbon(aes(x = t, ymin = .conf.low, ymax = .conf.up), linetype = 2, alpha = 0.1) +
  ggtitle("Model residuals (innovations)") +
  xlab("Time Step") + ylab("Count")




```



```{r MARSS-states-plot, fig.cap = "Time series of the states."}
############### Plot the time series of the states  #################
colr<-rainbow(numVars)
op<-par(mfrow=c(numVars,1), mar=c(4,4,0.1,0), oma=c(0,0,2,0.5))
for(i in 1:numVars) {
  mn = ts.ch.1$states[i,]
  se = ts.ch.1$states.se[i,]
  plot(years, mn, xlab="", ylab=Vars[i], bty="n", xaxt="n", type="n",
       ylim=c(min(mn-2*se, dat[i,], na.rm=TRUE), max(mn+2*se, dat[i,], na.rm=TRUE)))
  lines(years, rep(0,numYears), lty="dashed")
  lines(years, mn, col=colr[i], lwd=3)
  lines(years, mn+2*se, col=colr[i])
  lines(years, mn-2*se, col=colr[i])
  lines(years, dat[i,], pch=16, type='b', col="grey")
}
axis(1,at=seq(min(years),max(years)))
par(op)


```


```{r MARSS-covariances-plot, fig.cap = "MARSS fitted model variance/covariance matrix."}
# Look at variance/covariance
coef(ts.ch.1)$Q
Qmat <- matrix(0, numVars, numVars)
Qmat[lower.tri(Qmat, diag=TRUE)]<-ts.ch.1$par$Q
rownames(Qmat)<-Vars
colnames(Qmat)<-Vars
# Color coding scheme
rgb.palette.pos <- colorRampPalette(c("red", "white"), space = "rgb")
rgb.palette.neg <- colorRampPalette(c("white", "blue"), space = "rgb")
colours <- c("#000000",rgb.palette.pos(120), rep("#FFFFFF", 60), rgb.palette.neg(120), "#000000")
# Correlation matrix plot
lattice::levelplot(Qmat, scales=list(x=list(rot=90)), 
                   col.regions=colours, cuts=300, at=seq(-1,1,0.01), xlab="", ylab="")
```


```{r MARSS-covariance-matrix}
Qmat

```

# Data simulation

We take the fitted MARSS model and use it to forward simulate data. We can repeat this process to generate time series for life cycle model simulations.
 
```{r simulate-data}
simYears<-110
numSims<-100

# 
mySims <- MARSSsimulate(ts.ch.1, tSteps = simYears, nsim = numSims)

```

```{r diagnostics-one-simulation-plot, fig.cap = "Example simuation (simulation 1) from fitted MARSS model."}

plot(1:simYears, mySims$sim.data[3,,1], type="l", col="orangered", 
      ylab="Simulated response",
     ylim=c(-4,4))
lines(1:simYears, mySims$sim.data[2,,1], col="blue")
lines(1:simYears, mySims$sim.data[3,,1], col="brown")
lines(1:simYears, mySims$sim.data[1,,1], col="forestgreen")
legend("bottomleft", legend=c("low.q","peak.q.sthd","airTMAX", "peak.q"),#c("Porter", "GM"), 
       lty=1, lwd=4,
       ncol=3, bty="n",
       col=c("orangered", "blue", "brown", "forestgreen"))

```


```{r diagnostics-lowQ-plot, fig.cap = "Low flow Q7."}
plot(1:43, dat[3,], type="l", col=NA, main="Porter low Q",
     ylim=c(-4,4), xlim=c(1,105))
for(i in 1:numSims){
lines(1:105, mySims$sim.dat[3,1:105,i], col="blue")
}
lines(1:43, dat[3,], col="black", lwd=2)

```


```{r diagnostics-peakQ-plot, fig.cap = "Peak flow Q7."}
plot(1:43, dat[1,], type="l", col=NA, main="Porter peak Q coho & chinook",
     ylim=c(-4,4), xlim=c(1,105))
for(i in 1:numSims){
lines(1:105, mySims$sim.dat[1,1:105,i], col="orangered")
}
lines(1:43, dat[1,], col="black", lwd=2)
```

```{r diagnostics-peakQ-plot-sthd, fig.cap = "Peak flow Q7."}
plot(1:43, dat[2,], type="l", col=NA, main="Porter peak Q steelhead",
     ylim=c(-4,4), xlim=c(1,105))
for(i in 1:numSims){
lines(1:105, mySims$sim.dat[2,1:105,i], col="forestgreen")
}
lines(1:43, dat[2,], col="black", lwd=2)
```

```{r diagnostics-airTMAX-plot, fig.cap = "Air TMAX."}

plot(1:43, dat[3,], type="l", col=NA, main="Chehalis air TMAX",
     ylim=c(-4,4), xlim=c(1,105))
for(i in 1:numSims){
lines(1:105, mySims$sim.dat[3,1:105,i], col="brown")
}
lines(1:43, dat[3,], col="black", lwd=2)
```


```{r distribution-lowQ-plot, fig.cap = "Low flow."}

plot(density(dat[3,]), col="black", lwd=3, main="low Q",
     ylim=c(0,0.6))
for(i in 1:numSims){
lines(density(mySims$sim.data[3,,i]), col="blue")
}
lines(density(dat[3,]), col="black", lwd=3)
```

```{r distribution-peakQ-plot, fig.cap = "Peak flow."}

plot(density(dat[1,]), col="black", lwd=3, main="peak Q coho & chinook",
     ylim=c(0,0.6))
for(i in 1:numSims){
lines(density(mySims$sim.data[1,,i]), col="orangered")
}
lines(density(dat[1,]), col="black", lwd=3)
```

```{r distribution-peakQ-plot-sthd, fig.cap = "Peak flow."}

plot(density(dat[2,]), col="black", lwd=3, main="peak Q sthd",
     ylim=c(0,0.6))
for(i in 1:numSims){
lines(density(mySims$sim.data[2,,i]), col="orangered")
}
lines(density(dat[2,]), col="black", lwd=3)
```

```{r distribution-airTMAX-plot, fig.cap = "Air TMAX."}

plot(density(dat[3,]), col="black", lwd=3, main="air TMAX",
     ylim=c(0,0.6))
for(i in 1:numSims){
lines(density(mySims$sim.data[3,,i]), col="brown")
}
lines(density(dat[3,]), col="black", lwd=3)
```


```{r plot-diagnostics2, fig.width=7, fig.height=9, eval=FALSE, echo=FALSE}
# 1:1 plots for 10 simulations
par(mfrow=c(5,2))
for(i in 1:10){
  plot(mySims$sim.dat[1,,i], mySims$sim.dat[3,,i],
       xlab="GM", ylab="Porter",
       main=paste("Simuation num ", i))
}
```

```{r reverse-transform}
#https://stackoverflow.com/questions/46397553/r-transform-scaled-and-centered-data-to-original-values
# value * 'scaled:scale' + 'scaled:center'
# = value*std + mean
# lowQ, peakQ, airTMAX
# back-transform lowQ, peakQ
test <- apply(mySims$sim.data, MARGIN=c(2,3), function(x) x * t(c(porter.std, air.T.std)) + 
                t(c(porter.mean, air.T.mean)))
# 
test2 <- test
# one more step:
#  inverse log transform of Qmax
for(i in 1:dim(test2)[3]){
test2[1,,i] <- exp(test2[1,,i])
test2[2,,i] <- exp(test2[2,,i])
}
## check:              
test2[,1:5,1]
```


```{r check-back-transform-peakQ-plot, fig.cap = "Peak flow back-transformed with model simulations."}
plot(density(exp(dat[1,]*porter.std[1] + porter.mean[1])), 
     col="black", lwd=3, 
     main="Peak flow coho and chinook",
     ylim=c(0,0.00005)
     )
for(i in 1:numSims){
lines(density(test2[1,,i]), col="brown")
}
lines(density(exp(dat[1,]*porter.std[1] + porter.mean[1])), col="black", lwd=3)

```

```{r check-back-transform-peakQ-plot2, fig.cap = "Peak flow back-transformed with model simulations."}
plot(test2[1,,], 
     col = "black", 
     lwd = 3, 
     main = "Peak flow coho and chinook",
     ylim = c(0,0.00005),
     xlim = c(0, 60000))

for(i in 1:numSims){
  lines(density(test2[1,,i]), col="brown")
}

lines(density(test2[1,,]), col="black", lwd=3)

```

```{r check-back-transform-peakQ-plot-sthd, fig.cap = "Peak flow back-transformed with model simulations --  steelhead peak flows."}
plot(density(exp(dat[2,]*porter.std[2] + porter.mean[2])), 
     col="black", lwd=3, 
     main="Peak flow steelhead",
     ylim=c(0,0.00005)
     )
for(i in 1:numSims){
lines(density(test2[2,,i]), col="forestgreen")
}
lines(density(exp(dat[2,]*porter.std[2] + porter.mean[2])), col="black", lwd=3)

```
```{r check-back-transform-peakQ-sthd-plot2, fig.cap = "Peak flow back-transformed with model simulations."}
plot(test2[2,,], 
     col = "black", 
     lwd = 3, 
     main = "Peak flow sthd",
     ylim = c(0,0.00005),
     xlim = c(0, 60000))

for(i in 1:numSims){
  lines(density(test2[2,,i]), col="brown")
}

lines(density(test2[2,,]), col="black", lwd=3)

```

# Add data to `crp` package

Store the unscaled version of the three MARSS forecasts. This makes them available for other functions to call. 

```{r}
dat_marss <- test2

dimnames(dat_marss) <- list(#var = rownames(mySims$MLEobj$model$data),
                            var = c('q_peak', 'q_peak_sthd', 'q_low', 't_peak'),
                            year = 1:simYears,
                            run = 1:numSims)


# Convert array to dataframe
df_marss <- as.data.frame.table(dat_marss) %>%
  rename(value = Freq)


usethis::use_data(df_marss,
                  overwrite = TRUE)
```

