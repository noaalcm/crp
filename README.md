
# Chehalis Resilience Paper

[![Release
version](https://img.shields.io/badge/version-0.2.27-blue)](https://bitbucket.org/noaalcm/crp)
[![CircleCI](https://circleci.com/bb/noaalcm/crp.svg?style=shield&circle-token=d9bc509b837cadbd952cf990ef6fb10237a49d4c)](https://app.circleci.com/pipelines/bitbucket/noaalcm/crp)
[![codecov](https://codecov.io/bb/noaalcm/crp/branch/master/graph/badge.svg?token=HSZFF2RXEZ)](https://codecov.io/bb/noaalcm/crp)

Chehalis Resilience Paper is where we are holding the analysis of
resilience and stochasticity for the Chehalis basin salmon life cycle
models.

## Intended usage

At a high level there are three functions which need to be used to
generate `crp` model results

``` r
# deterministic
hab_run('coho', 'Current', 2019) %>%  
  lcm_assign() %>%  
  lcm_run() 

# stochastic
hab_run_stochastic('coho', 'Current', 2019, runs = 1:2, years = 1:100) %>%  
  lcm_assign() %>%  
  lcm_run_iteratively() 
```

# Examples

## Deterministic

Create all habitat scenarios for a given species, scenario, and era

``` r
hab_run('coho', 'Current', 2019)# store habitat parameters using save_hab = TRUE
```

Run the LCM for a specific species and scenario

``` r
p <- lcm_assign(h)

lcm_run(p) %>% 
  filter(lifestage == 'spawners', year == 100) %>% 
  mutate_if(is.numeric, round) %>%
  head(10)
```

If you want to see the equilibrium values (like the ASRP results) subset
the results to only the spawners in year 100.

``` r
hab_run('coho', 'Historical', 1900) %>%
  lcm_assign() %>%
  lcm_run() %>% 
  filter(lifestage == 'spawners', year == 100) %>% 
  mutate(n = round(n)) %>% 
  head(5)
```

Or for spring chinook

``` r
hab_run('spring_chinook', 'Floodplain', 2019) %>%
  lcm_assign() %>%
  lcm_run() %>% 
  head(3)
```

## Stochastic

We can use data from our MARSS simulations to create stochastic runs.
Here we simulate 20 years of data for one run. 10 years ar used for
initialization and not shown. Generally years will be set to 110, which
is the length of the MARSS simulations. Use `lcm_run_iteratively()` with
`hab_run_stochastic`.

``` r
h_1_20 <- hab_run_stochastic('spring_chinook', 
                             'Current', 
                             2019,
                             runs = 1,
                             years = 1:20)

lcm_assign(h_1_20) %>%
  lcm_run_iteratively() %>%
  filter(lifestage == 'spawners',
         n > 0) %>%
  ggplot(aes(as.numeric(year), n, color = subbasin)) +
  geom_line() +
  theme_bw() +
  labs(x = 'model year', 
       y = 'spawners', 
       title = 'Spring Chinook - Current')
```

``` r
hab_run_stochastic('spring_chinook', 
                   'Current', 
                   2019,
                   run = 1:2,
                   year = 1:3) %>%
  lcm_assign() %>%
  lcm_run_iteratively()
```

## Resilience paper scenarios

To generate an HTML output report of crp scenarios run
`inst/scripts/run-resulence-scenarios.R`. By default it saves the html
report to `ignore/run_resilience_scenarios`, but if run on the `master`
branch a copy will be saved to the shared drive.

Running this as a job with many cores (up to 64) on the hydra server is
how it is intended to be used.To run the scenarios in parallel, use
`hab_run_stochastic` with the `furrr::` family of functions.

A small example

``` r
library(furrr)
plan(multisession, workers = 2)

crossing(pop = pops_all[1],
         hab_scenario = scenarios_all[1],
         era = eras_all[2],
         runs = 1:2,
         years = 1:2) %>%
  future_pmap_dfr(hab_run_stochastic)
```

# Installation

The repository is located here: <https://bitbucket.org/noaalcm/crp/>

To work on the model, clone the repository, open the R project and run
`devtools::build()`

    git clone https://bitbucket.org/noaalcm/crp/
